const jwt = require("jsonwebtoken")
const secret = "IDoNotKnow" 
module.exports.createAccessToken = (user) => {
 
    const data = {
        id : user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

  
    return jwt.sign(data, secret, {})
}
// token verification 
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization 
    if (typeof token !== "undefined") {
        console.log(token)
        token = token.slice(7, token.length)
     
        return jwt.verify(token,secret,(err,data) => {
            if(err){
                return res.send({auth: "failed"})
            } else {
                // tells the server to proced to processing of the request
                next();
            }
        })
    }
}

// token decoding 

module.exports.decode = (token) => {
    if (typeof token !== "unde fined"){
        token = token.slice(7, token.length)
        return jwt.verify(token, secret, (err,data) => {
            if (err) {
                console.log(err)
                return null 
            } else {
                return jwt.decode(token, {complete: true}).payload
                // jwt.decode - decides the data to be decoded, which is the toke,
                // payload - the one that we need ot verify the user informatio this is a par tof the token when the code the createAccessTOken function(the on with the _id, email, and is amdin)
            }
        })      
    } else {
        return null // there is no token
    }
}