const mongoose = require("mongoose")

/* 


order 
totalAmount number
purchasedOn (date)
must be assoc with (user who owns the order products that belong to the order)

*/

const orderSchema = new mongoose.Schema({
    userId : { 
        type : String,
        required : true
    },
    products : [
        {
            productId:{
                type: String,
            },
            quantity:{
                type: Number,
                default : 1,
            },
        }, 
    ],
    amount : {type: Number, required: [true, "Require total number"]},
    address : {type: Object, required: true},
    status : {type: String, default: "pending"},
    
},

{timestamps:true}

)

module.exports = mongoose.model("Order", orderSchema)