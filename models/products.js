const mongoose = require ("mongoose")

/* 
product
name (strng)
descriptioon (string)
price (number)
isActive (date)
created on (date)

*/

const productSchema = new mongoose.Schema({
    productName:{
        type : String,
        required: [true, "Product Name is Required"],
        unique : true
    },
    description: { 
        type: String, 
        required: [true, "Product description is required"]
    },
    price:{
        type: Number,
        required : [true, "Price for the course is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },

},
{timestamps : true}
)

module.exports = mongoose.model("Product", productSchema)