const mongoose = require ("mongoose")

/* 
User
Name string
email string
password string
is admin bool
*/

const userSchema = new mongoose.Schema({
    userName : {
        type: String,
        required : [true, "User Name is Required"],
        unique : true
    },
    email:{
        type: String,
        required : [true, "Email  is Required"]
    },
    password:{
        type: String,
        required : [true, "Password is Required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    
},
{timestamps:true}
)

module.exports = mongoose.model("User", userSchema)