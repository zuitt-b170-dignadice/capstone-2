// set up depencies

const User = require("../models/user.js")
const Products = require("../models/products.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") // used to encrypt user passwords

// check if email exists
module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				// return result
				return true
			} else {
				// return res.send("email does not exist")
				return false
			}
		}
	})
}

// Account creation
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({

        /* 
        userName 
        email
        password
        isAdmin

        */

        userName : reqBody.userName,
        email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin : reqBody.isAdmin
    
	})
	return newUser.save().then((saved, error) =>{
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

// user login
module.exports.userLogin = (reqBody) => {
    return User.findOne ({email: reqBody.email}).then(result => {
        if (result === null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if (isPasswordCorrect) {
                return {access: auth.createAccessToken(result.toObject())}
            } else {
                return false
            }
        }
    })
}


// get profile

module.exports.getProfile = (data) => {
	return User.findById(data.userId)
	.then(result => {
		if (result === null) {
			return false
		} else {
			result.password = ""
			return result
		}
	})
}

// make admin 





