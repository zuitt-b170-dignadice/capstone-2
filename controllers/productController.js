const User = require("../models/user.js")
const Product = require("../models/products.js")


module.exports.addProduct = (reqBody, userData) => {
    return User.findById(userData.userId)
    .then(result => {
        if (userData.isAdmin == false) {
            return "You cannot do that"
        } else {
            let newProduct = new Product({
                productName: reqBody.productName,
                description: reqBody.description,
                price: reqBody.price,
                isActive: reqBody.isActive
            })
            return newProduct.save()
            .then((product, error) => {
                if(error){
                    return false
                } else {
                    return "Product Added Successfully"
                }
            })
        }
    })
}

// get all products

module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result
    })
}

// get 1 product 

module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.courseId).then(result => {
        return result
    })
}


module.exports.archiveProduct = (reqParams, reqBody ) => {
	let updatedProduct = {
		isActive: reqBody.isActive //  true or false will aslo do
    }
	return Course.findByIdAndUpdate(reqParams.courseId, updatedProduct).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})

}

