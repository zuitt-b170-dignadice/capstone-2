

const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController")
const user = require("../models/user.js")


router.post("/createAccount", (req, res) => {
    userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController))
}) // localhost:3000/api/users/createAccount 

router.post("/login", (req, res) => {
    userController.userLogin(req.body)
    .then(resultFromController => res.send(resultFromController))
}) // localhost:3000/api/users/login


router.get("/details", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
}) // localhost:3000/api/users/details

// create accoutn > /login > copy token > /details > paste token

// makeAdmin 

/* 
 when token is pasted on the API, the userID becomes ADMIN 
*/


module.exports = router;