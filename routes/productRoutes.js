const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productController")
const user = require("./userRoutes.js")


// app.use("/api/products", productRoutes)
// localho

router.post("/addProduct", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.addProduct(req.body, userData).
    then(resultFromController => res.send(resultFromController))
})

/* 

   productName: reqBody.productName,
   description: reqBody.description,
   price: reqBody.price,
 isActive: reqBody.isActive


*/

router.get("/:productId",  (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params)
    .then(result => res.send(result))
})









module.exports = router;




