const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// access to routes
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")

// server
const app = express();
const port = 3000 

app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use("/api/users", userRoutes)
app.use("/api/products", productRoutes)
app.use("/api/orders", orderRoutes)

mongoose.connect("mongodb+srv://admin:admin@cluster0.h8y6g.mongodb.net/Capstone_2?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
})


let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("we're connected to the database"))

/* app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT || port}`)) */

app.listen(port, () => console.log(`API now online at port ${port}`))
